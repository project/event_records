<?php

/**
 * @file
 * Builds placeholder replacement tokens for "event record"-related data.
 */

use Drupal\event_records\EventRecordInterface;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function event_records_token_info() {
  $type = [
    'name' => t('Event records'),
    'description' => t('Tokens related to individual event record items.'),
    'needs-data' => 'event_record',
  ];

  $event_record = [];

  // Core tokens for event records.
  $event_record['id'] = [
    'name' => t("Event record ID"),
    'description' => t('The unique ID of the event record item.'),
  ];
  $event_record['type'] = [
    'name' => t("Machine name of the event record type"),
  ];
  $event_record['type_label'] = [
    'name' => t("Human-readable label of the event record type."),
    'description' => t("The human-readable name of the event record type."),
  ];
  $event_record['label'] = [
    'name' => t("Label"),
  ];

  // Chained tokens for event records.
  $event_record['created'] = [
    'name' => t("Date created"),
    'type' => 'date',
  ];

  // Miscellaneous tokens for event records.
  $event_record['string_representation'] = [
    'name' => t("String representation"),
    'description' => t("A generic string representation of this event record. Warning: This might expose undesired field content."),
  ];

  return [
    'types' => ['event_record' => $type],
    'tokens' => ['event_record' => $event_record],
  ];
}

/**
 * Implements hook_tokens().
 */
function event_records_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  if ($type !== 'event_record' || empty($data['event_record']) || !($data['event_record'] instanceof EventRecordInterface)) {
    return $replacements;
  }

  $token_service = \Drupal::token();

  /** @var \Drupal\event_records\EventRecordInterface $event_record */
  $event_record = $data['event_record'];

  if (isset($options['langcode'])) {
    $langcode = $options['langcode'];
  }
  else {
    $langcode = LanguageInterface::LANGCODE_DEFAULT;
  }

  $bubbleable_metadata->addCacheableDependency($event_record);
  if ($type_id = $event_record->bundle()) {
    if ($event_record_type = \Drupal::entityTypeManager()->getStorage('event_record_type')->load($type_id)) {
      $bubbleable_metadata->addCacheableDependency($event_record_type);
    }
  }

  foreach ($tokens as $name => $original) {
    switch ($name) {
      case 'id':
        $replacements[$original] = $event_record->id();
        break;

      case 'type':
        $replacements[$original] = $type_id;
        break;

      case 'type_label':
        $replacements[$original] = !empty($event_record_type) ? t($event_record_type->label()) : '';
        break;

      case 'label':
        $replacements[$original] = $event_record->label();
        break;

      // Default values for the chained tokens handled below.
      case 'created':
        $date_format = DateFormat::load('medium');
        $bubbleable_metadata->addCacheableDependency($date_format);
        $replacements[$original] = \Drupal::service('date.formatter')->format($event_record->getCreatedTime(), 'medium', '', NULL, $langcode);
        break;

      case 'string_representation':
        $replacements[$original] = $event_record->getStringRepresentation();
        break;
    }
  }

  if ($created_tokens = $token_service->findWithPrefix($tokens, 'created')) {
    $replacements += $token_service->generate('date', $created_tokens, ['date' => $event_record->getCreatedTime()], $options, $bubbleable_metadata);
  }

  return $replacements;
}
