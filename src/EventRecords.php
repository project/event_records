<?php

namespace Drupal\event_records;

use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Static methods regarding event records.
 */
final class EventRecords {

  /**
   * Returns a list of allowed event categories for a category field.
   *
   * @param \Drupal\Core\Field\FieldStorageDefinitionInterface $definition
   *   The according field storage definition.
   *
   * @return array
   *   The allowed categories. Keyed by machine name, values are human-readable
   *   and translatable labels.
   */
  public static function categories(FieldStorageDefinitionInterface $definition): array {
    $categories = [];
    \Drupal::moduleHandler()->alter('event_record_categories', $categories, $definition);
    return $categories;
  }

}
