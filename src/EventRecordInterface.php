<?php

namespace Drupal\event_records;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for event records.
 */
interface EventRecordInterface extends ContentEntityInterface {

  /**
   * Gets the event record label.
   *
   * @return string
   *   Label of the event record.
   */
  public function getLabel();

  /**
   * Sets the event record label.
   *
   * @param string $label
   *   The event record label.
   *
   * @return \Drupal\event_records\EventRecordInterface
   *   The called event record entity.
   */
  public function setLabel($label);

  /**
   * Gets the event record creation timestamp.
   *
   * @return int
   *   Creation timestamp of the event record.
   */
  public function getCreatedTime();

  /**
   * Sets the event record creation timestamp.
   *
   * @param int $timestamp
   *   The event record creation timestamp.
   *
   * @return \Drupal\event_records\EventRecordInterface
   *   The called event record entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Get a brief string representation of this event record.
   *
   * The returned string has a maximum length of 255 characters.
   * Warning: This might expose undesired field content.
   *
   * This method is not implemented as __toString(). Instead it is this method
   * name, to guarantee compatibility with future changes of the Entity API.
   * Another reason is, that this method is kind of a last resort for generating
   * the event record label, and is not supposed to be used for other purposes
   * like serialization.
   *
   * Modules may implement hook_event_record_get_string_representation() to
   * change the final result, which will be returned by this method.
   *
   * @return string
   *   The string representation of this event record.
   */
  public function getStringRepresentation(): string;

  /**
   * Applies a label pattern to update the label property.
   *
   * Developers may define a custom label pattern by setting a public
   * "label_pattern" as string property or field. If it is not set, then the
   * configured label pattern in the corresponding type config will be used.
   *
   * @param array $token_data
   *   (optional) Data to use for Token replacement.
   */
  public function applyLabelPattern(array $token_data = []): void;

  /**
   * Get the according event record type (i.e. the bundle as object).
   *
   * @return \Drupal\event_records\EventRecordTypeInterface
   *   The event record type as object.
   */
  public function getType(): EventRecordTypeInterface;

}
