<?php

namespace Drupal\event_records;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Interface for an event record type.
 */
interface EventRecordTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {

  /**
   * Set the default status value.
   *
   * @param bool $status
   *   The default status value.
   *
   * @return $this
   */
  public function setStatus($status): EventRecordTypeInterface;

  /**
   * Get the pattern to use for creating the label of the event record.
   *
   * @return string
   *   The label pattern.
   */
  public function getLabelPattern(): string;

  /**
   * Set the pattern to use for creating the label of the event record.
   *
   * @param string $pattern
   *   The label pattern to set.
   *
   * @return $this
   */
  public function setLabelPattern($pattern): EventRecordTypeInterface;

}
