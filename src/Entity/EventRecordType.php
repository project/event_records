<?php

namespace Drupal\event_records\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\event_records\EventRecordTypeInterface;

/**
 * Defines event record types as configuration entities.
 *
 * @ConfigEntityType(
 *   id = "event_record_type",
 *   label = @Translation("Event record type"),
 *   label_collection = @Translation("Event record types"),
 *   label_singular = @Translation("event record type"),
 *   label_plural = @Translation("event record types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count event record type",
 *     plural = "@count event record types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\event_records\Form\EventRecordTypeForm",
 *       "edit" = "Drupal\event_records\Form\EventRecordTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\event_records\EventRecordTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer event record types",
 *   bundle_of = "event_record",
 *   config_prefix = "event_record_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/event_record_types/add",
 *     "edit-form" = "/admin/structure/event_record_types/manage/{event_record_type}",
 *     "delete-form" = "/admin/structure/event_record_types/manage/{event_record_type}/delete",
 *     "collection" = "/admin/structure/event_record_types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "uuid",
 *     "status",
 *     "label_pattern"
 *   }
 * )
 */
class EventRecordType extends ConfigEntityBundleBase implements EventRecordTypeInterface {

  /**
   * The machine name of this event record type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the event record type.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of this event record type.
   *
   * @var string
   */
  protected $description;

  /**
   * Whether event records should be automatically created.
   *
   * @var bool
   */
  protected $status = TRUE;

  /**
   * A pattern to use for creating the label of the event record.
   *
   * @var string
   */
  protected string $label_pattern = '[event_record:string_representation]';

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    return $this->set('description', $description);
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status): EventRecordTypeInterface {
    $this->status = (bool) $status;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabelPattern(): string {
    return $this->label_pattern;
  }

  /**
   * {@inheritdoc}
   */
  public function setLabelPattern($pattern): EventRecordTypeInterface {
    $this->label_pattern = $pattern;
    return $this;
  }

}
