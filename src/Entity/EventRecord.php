<?php

namespace Drupal\event_records\Entity;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\event_records\EventRecordInterface;
use Drupal\event_records\EventRecordTypeInterface;

/**
 * Defines event records as content entities.
 *
 * @ContentEntityType(
 *   id = "event_record",
 *   label = @Translation("Event record"),
 *   label_collection = @Translation("Event records"),
 *   label_singular = @Translation("event record"),
 *   label_plural = @Translation("event records"),
 *   label_count = @PluralTranslation(
 *     singular = "@count event record",
 *     plural = "@count event records",
 *   ),
 *   bundle_label = @Translation("Event record type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\event_records\EventRecordListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\event_records\Form\EventRecordForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "query_access" = "Drupal\entity\QueryAccess\QueryAccessHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "views_data" = "Drupal\entity\EntityViewsData"
 *   },
 *   base_table = "event_record",
 *   admin_permission = "administer event_record",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/event-record/add/{event_record_type}",
 *     "add-page" = "/admin/content/event-record/add",
 *     "canonical" = "/event-record/{event_record}",
 *     "collection" = "/admin/content/event-record"
 *   },
 *   bundle_entity_type = "event_record_type",
 *   field_ui_base_route = "entity.event_record_type.edit_form",
 *   token_type = "event_record"
 * )
 */
class EventRecord extends ContentEntityBase implements EventRecordInterface {

  /**
   * {@inheritdoc}
   *
   * When a new event record entity is created, set the uid entity reference to
   * the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $time = \Drupal::time()->getCurrentTime();
    $values += [
      'created' => $time,
      'changed' => $time,
      'uid' => \Drupal::currentUser()->id(),
      'source' => 'auto',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->get('label')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLabel($label) {
    $this->set('label', $label);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Label'))
      ->setDescription(t('The label of the event record entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setLabel(t('User'))
      ->setDescription(t('The ID of the user that was logged in when this event was created.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the event record was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the event record was last edited.'));

    $fields['source'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Creation source'))
      ->setDescription(t('Whether this record was created automatically via hooks, event listeners, UI form or something else.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', FALSE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getStringRepresentation(): string {
    $string = '';
    \Drupal::moduleHandler()->invokeAllWith('event_record_get_string_representation', function (callable $hook, string $module) use (&$string) {
      $string = $hook($this, $string);
    });

    if (trim($string) === '') {
      $string = $this->generateFallbackStringRepresentation();
    }

    if (mb_strlen($string) > 255) {
      $string = Unicode::truncate($string, 255, TRUE, TRUE, 20);
    }

    return $string;
  }

  /**
   * Fallback method for generating a string representation.
   *
   * @see ::getStringRepresentation()
   *
   * @return string
   *   The fallback value for the string representation.
   */
  protected function generateFallbackStringRepresentation() {
    $components = \Drupal::service('entity_display.repository')->getFormDisplay('event_record', $this->bundle())->getComponents();

    // The label is available in the form, thus the user is supposed to enter
    // a value for it. For this case, use the label directly and return it.
    if (!empty($components['label'])) {
      return $this->label();
    }

    uasort($components, 'Drupal\Component\Utility\SortArray::sortByWeightElement');

    /** @var \Drupal\Core\Datetime\DateFormatterInterface $date_formatter */
    $date_formatter = \Drupal::service('date.formatter');
    $time = $this->getCreatedTime() ?: \Drupal::time()->getCurrentTime();
    $description = t('@type on @time by @user', [
      '@type' => $this->getType()->label(),
      '@time' => $date_formatter->format($time),
      '@user' => $this->uid->entity ? $this->uid->entity->label() : t('anonymous'),
    ]);
    $values = [];
    foreach (array_keys($components) as $field_name) {
      // Components can be extra fields, check if the field really exists.
      if (!$this->hasField($field_name)) {
        continue;
      }
      $field_definition = $this->getFieldDefinition($field_name);

      // Only take care for accessible string fields.
      if (!($field_definition instanceof FieldConfigInterface) || $field_definition->getType() !== 'string' || !$this->get($field_name)->access('view')) {
        continue;
      }

      if ($this->get($field_name)->isEmpty()) {
        continue;
      }

      foreach ($this->get($field_name) as $field_item) {
        $values[] = $field_item->value;
      }

      // Stop after two value items were received.
      if (count($values) > 2) {
        return implode(' ', array_slice($values, 0, 2)) . '...';
      }
    }

    if (!empty($values)) {
      $description .= ': ';
    }
    return trim($description . implode(' ', $values));
  }

  /**
   * Implements the magic __toString() method.
   *
   * When a string representation is explicitly needed, consider directly using
   * ::getStringRepresentation() instead.
   */
  public function __toString() {
    return $this->getStringRepresentation();
  }

  /**
   * {@inheritdoc}
   */
  public function applyLabelPattern(array $token_data = []): void {
    if (isset($this->label_pattern)) {
      $label_pattern = $this->hasField('label_pattern') ? $this->get('label_pattern')->getString() : $this->label_pattern;
    }
    elseif ($type_id = $this->bundle()) {
      /** @var \Drupal\event_records\EventRecordTypeInterface $type */
      if ($type = \Drupal::entityTypeManager()->getStorage('event_record_type')->load($type_id)) {
        $label_pattern = $type->getLabelPattern();
      }
    }
    if (!empty($label_pattern)) {
      $string = (string) \Drupal::token()->replace($label_pattern, $token_data + ['event_record' => $this], [
        'langcode' => $this->language()->getId(),
        'clear' => TRUE,
      ]);
      if (mb_strlen($string) > 255) {
        $string = Unicode::truncate($string, 255, TRUE, TRUE, 20);
      }
      $this->label->value = $string;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getType(): EventRecordTypeInterface {
    return \Drupal::entityTypeManager()->getStorage('event_record_type')->load($this->bundle());
  }

}
