<?php

namespace Drupal\event_records\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for event record type forms.
 */
class EventRecordTypeForm extends BundleEntityFormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->setEntityTypeManager($container->get('entity_type.manager'));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\event_records\EventRecordTypeInterface $event_record_type */
    $event_record_type = $this->entity;
    if ($this->operation == 'add') {
      $form['#title'] = $this->t('Add event record type');
    }
    else {
      $form['#title'] = $this->t(
        'Edit type of %label event records',
        ['%label' => $event_record_type->label()]
      );
    }

    $form['label'] = [
      '#title' => $this->t('Type label'),
      '#type' => 'textfield',
      '#default_value' => $event_record_type->label(),
      '#description' => $this->t('The human-readable name of this event record type.'),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $event_record_type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => ['Drupal\event_records\Entity\EventRecordType', 'load'],
        'source' => ['label'],
      ],
      '#description' => $this->t('A unique machine-readable name for this event record type. It must only contain lowercase letters, numbers, and underscores.'),
    ];

    $form['description'] = [
      '#title' => $this->t('Description'),
      '#type' => 'textarea',
      '#default_value' => $event_record_type->getDescription(),
      '#description' => $this->t('Describe this type of event records. The text will be displayed on the <em>Add new event record</em> page.'),
    ];

    $form['label_pattern'] = [
      '#title' => $this->t('Label pattern'),
      '#type' => 'textfield',
      '#description' => $this->t('Define a pattern to use for creating the label of the event record. Tokens are allowed, e.g. [event_record:string_representation]. Leave empty to not use any pattern.'),
      '#default_value' => $event_record_type->getLabelPattern(),
      '#size' => 255,
      '#maxlength' => 255,
    ];

    if ($this->moduleHandler->moduleExists('token')) {
      $form['label_pattern_help'] = [
        '#type' => 'container',
        'token_link' => [
          '#theme' => 'token_tree_link',
          '#token_types' => ['event_record'],
          '#dialog' => TRUE,
        ],
      ];
    }
    else {
      $form['label_pattern']['#description'] .= ' ' . $this->t('To get a list of available tokens, install the <a target="_blank" rel="noreferrer noopener" href=":drupal-token" target="blank">contrib Token</a> module.', [':drupal-token' => 'https://www.drupal.org/project/token']);
    }

    $form['additional_settings'] = [
      '#type' => 'vertical_tabs',
    ];

    $form['workflow'] = [
      '#type' => 'details',
      '#title' => $this->t('Workflow'),
      '#group' => 'additional_settings',
    ];

    $form['workflow']['options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Default options'),
      '#default_value' => $this->getWorkflowOptions(),
      '#options' => [
        'status' => $this->t('Automatic creation enabled'),
      ],
    ];
    $form['workflow']['options']['status']['#description'] = $this->t('When checked, event records will be automatically created at according system events.');

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save this type');
    $actions['delete']['#value'] = $this->t('Delete this type');
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\event_records\EventRecordTypeInterface $event_record_type */
    $event_record_type = $this->entity;

    $event_record_type->set('id', trim($event_record_type->id()));
    $event_record_type->set('label', trim($event_record_type->label()));
    $event_record_type->set('status', (bool) $form_state->getValue(['options', 'status']));
    $status = $event_record_type->save();

    $t_args = ['%name' => $event_record_type->label()];
    if ($status == SAVED_UPDATED) {
      $message = $this->t('The type for event records %name has been updated.', $t_args);
    }
    elseif ($status == SAVED_NEW) {
      $message = $this->t('The type for event records %name has been added.', $t_args);
    }
    $this->messenger()->addStatus($message);

    $form_state->setRedirectUrl($event_record_type->toUrl('collection'));
  }

  /**
   * Prepares workflow options to be used in the 'checkboxes' form element.
   *
   * @return array
   *   Array of options ready to be used in #options.
   */
  protected function getWorkflowOptions() {
    /** @var \Drupal\event_records\EventRecordTypeInterface $event_record_type */
    $event_record_type = $this->entity;
    $workflow_options = [
      'status' => $event_record_type->status(),
    ];
    // Prepare workflow options to be used for 'checkboxes' form element.
    $keys = array_keys(array_filter($workflow_options));
    return array_combine($keys, $keys);
  }

}
