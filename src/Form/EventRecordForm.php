<?php

namespace Drupal\event_records\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\event_records\EventRecordInterface;

/**
 * Form controller for the event record entity edit forms.
 */
class EventRecordForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $form['#entity_builders']['apply_label_pattern'] = [
      static::class,
      'applyLabelPattern',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\event_records\EventRecordInterface $entity */
    $entity = $this->getEntity();
    $entity->get('source')->setValue('form:' . $this->getFormId());
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => \Drupal::service('renderer')->render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New event record %label has been created.', $message_arguments));
      $this->logger('event_records')->notice('Created new event record %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The event record %label has been updated.', $message_arguments));
      $this->logger('event_records')->notice('Updated new event record %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.event_record.collection');
  }

  /**
   * Entity builder callback that applies the label pattern.
   *
   * @param string $entity_type_id
   *   The entity type identifier.
   * @param \Drupal\event_records\EventRecordInterface $entity
   *   The entity updated with the submitted values.
   * @param array $form
   *   The complete form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function applyLabelPattern($entity_type_id, EventRecordInterface $entity, array $form, FormStateInterface $form_state) {
    if (!isset($entity->original) && !$entity->isNew()) {
      // Load the unchanged values from the database in order to access
      // previous values.
      $entity->original = \Drupal::entityTypeManager()
        ->getStorage($entity->getEntityTypeId())->loadUnchanged($entity->id());
    }
    $entity->applyLabelPattern();
    // Disable the label pattern afterwards, in order to avoid redundant
    // rebuilds during the save operation chain.
    if ($entity->hasField('label_pattern')) {
      $entity->get('label_pattern')->setValue('');
    }
    else {
      $entity->label_pattern = '';
    }
  }

}
