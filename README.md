# Event Records

**Manage system events as persisted records.**

https://www.drupal.org/project/event_records

## 0. Contents

- 1. Introduction
- 2. Requirements
- 3. Installation
- 4. Usage
- 5. Maintainers
- 6. Support and contribute

## 1. Introduction

With Event Records, things that happen in your system may be persisted as
content entities. There may be multiple use cases for what event records
can be used.

## 2. Requirements

This module builds on top of contrib Entity API.
Therefore, the contrib Entity API (https://www.drupal.org/project/entity) is
required to be additionally installed besides Drupal core.

## 3. Installation

Install the module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

## 4. Usage

Most probably developers decide which events happening in the system should be
persisted, and thus developers may provide additional sub-modules which contain
the according types of event records.

You should also take a look at the permissions page at /admin/people/permissions
and make sure whether the configured permissions are properly set.

## 5. Maintainers

* Maximilian Haupt (mxh) - https://www.drupal.org/u/mxh

## 6. Support and contribute

To submit bug reports and feature suggestions, or to track changes visit:
https://www.drupal.org/project/issues/event_records

You can also use this issue queue for contributing, either by submitting ideas,
or new features and mostly welcome - patches and tests.
