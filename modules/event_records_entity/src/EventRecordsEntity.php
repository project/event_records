<?php

namespace Drupal\event_records_entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\event_records\EventRecordInterface;

/**
 * Static methods regarding event records for entities.
 */
final class EventRecordsEntity {

  /**
   * Returns a list of entity types for an entity type field.
   *
   * @param \Drupal\Core\Field\FieldStorageDefinitionInterface $definition
   *   The according field storage definition.
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   The according entity, if available.
   *
   * @return array
   *   The entity types. Keyed by machine name, values are human-readable
   *   and translatable labels.
   */
  public static function types(FieldStorageDefinitionInterface $definition, ?EntityInterface $entity = NULL): array {
    $etm = \Drupal::entityTypeManager();
    if ($entity instanceof EventRecordInterface) {
      $type_id = $entity->bundle();
      if (substr($type_id, 0, 7) === 'entity_') {
        $parts = explode('_', $type_id);
        while ($part = array_pop($parts)) {
          if ($etm->hasDefinition($part)) {
            $entity_type = $etm->getDefinition($part);
            break;
          }
        }
      }
    }

    $entity_types = [];
    if (isset($entity_type)) {
      $entity_types[$entity_type->id()] = $entity_type->getLabel();
    }
    else {
      foreach ($etm->getDefinitions() as $entity_type) {
        $entity_types[$entity_type->id()] = $entity_type->getLabel();
      }
    }
    return $entity_types;
  }

  /**
   * Returns a list of entity types for an entity type field.
   *
   * @param \Drupal\Core\Field\FieldStorageDefinitionInterface $definition
   *   The according field storage definition.
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   The according entity, if available.
   *
   * @return array
   *   The entity types. Keyed by machine name, values are human-readable
   *   and translatable labels.
   */
  public static function bundles(FieldStorageDefinitionInterface $definition, ?EntityInterface $entity = NULL): array {
    $etm = \Drupal::entityTypeManager();
    /** @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info */
    $bundle_info = \Drupal::service('entity_type.bundle.info');
    if ($entity instanceof EventRecordInterface) {
      $type_id = $entity->bundle();
      if (substr($type_id, 0, 7) === 'entity_') {
        $parts = explode('_', $type_id);
        while ($part = array_pop($parts)) {
          if ($etm->hasDefinition($part)) {
            $entity_type = $etm->getDefinition($part);
            break;
          }
        }
      }
    }

    $bundles = [];
    if (isset($entity_type)) {
      foreach ($bundle_info->getBundleInfo($entity_type->id()) as $bundle => $info) {
        $bundles[$bundle] = $info['label'] ?? $bundle;
      }
    }
    else {
      foreach ($etm->getDefinitions() as $entity_type) {
        foreach ($bundle_info->getBundleInfo($entity_type->id()) as $bundle => $info) {
          $bundles[$bundle] = $info['label'] ?? $bundle;
        }
      }
    }
    return $bundles;
  }

  /**
   * Returns a JSON-formatted string representation of the given entity.
   *
   * @return string
   *   The JSON string.
   */
  public static function toJson(EntityInterface $entity): string {
    /** @var \Symfony\Component\Serializer\Serializer $serializer */
    $serializer = \Drupal::service('serializer');
    return $serializer->serialize($entity, 'json', ['json_encode_options' => JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES]);
  }

}
