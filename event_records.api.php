<?php

/**
 * @file
 * Hooks specific to the Event Records module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Define a string representation for the given event record.
 *
 * In case the hook implementation returns an empty string, a fallback value
 * will be generated, or another module might generate the value.
 *
 * @param \Drupal\event_records\EventRecordInterface $event_record
 *   The event record.
 * @param string $string
 *   The current value of the string representation.
 *
 * @return string
 *   The generated string representation.
 *
 * @see \Drupal\event_records\EventRecordInterface::getStringRepresentation()
 */
function hook_event_record_get_string_representation(\Drupal\event_records\EventRecordInterface $event_record, $string) {
  if ($event_record->isNew()) {
    return 'NEW - ' . $event_record->get('my_custom_field')->value;
  }
  return $event_record->get('my_custom_field')->value;
}

/**
 * Alter the list of allowed event categories.
 *
 * This list will be used for selection on the event's category field.
 *
 * @param array &$categories
 *   The current list of categories. Keyed by machine name, values are
 *   human-readable and translatable labels.
 * @param \Drupal\Core\Field\FieldStorageDefinitionInterface $definition
 *   The according field storage definition.
 */
function hook_event_record_categories_alter(array &$categories, \Drupal\Core\Field\FieldStorageDefinitionInterface $definition) {
  // Add a "bubble" category.
  $categories['bubble'] = t('Bubble');
}

/**
 * @} End of "addtogroup hooks".
 */
